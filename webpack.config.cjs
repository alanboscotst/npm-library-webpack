const { resolve } = require("path");
const { version } = require('./package.json');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const WebpackShellPluginNext = require('webpack-shell-plugin-next');

const config = {
    target: "web",
    entry: {
        index: "./src/index.js",
    },
    output: {
        path: resolve(__dirname, "./dist"),
        filename: `npm-library-webpack-${version}.js`,
        library: "NpmLibraryWebpack",
        libraryTarget: "umd",
        globalObject: "this",
        umdNamedDefine: true,
    },
    watchOptions: {
        aggregateTimeout: 600,
        ignored: /node_modules/,
    },
    plugins: [
        new CleanWebpackPlugin({
            cleanStaleWebpackAssets: false,
            cleanOnceBeforeBuildPatterns: [resolve(__dirname, "./dist")],
        }),
        new WebpackShellPluginNext({
            onBuildEnd: {
                scripts: ['npm run pack'],
                blocking: false,
                parallel: true
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.js(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env"],
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: [".js"],
    },
};

module.exports = (env, argv) => {
    if (argv.mode === "development") {
    } else if (argv.mode === "production") {
    } else {
        throw new Error("Specify env");
    }

    return config;
};