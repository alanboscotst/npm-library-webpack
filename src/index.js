const printMsg = function () {
    return "This is a message from the main package";
}

function init() {
    if (typeof window !== 'undefined') {
        window.printMsg = printMsg;
    }
}

init()
export { printMsg }
export default printMsg